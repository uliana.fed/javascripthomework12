const buttons = document.querySelectorAll('.btn');

buttons.forEach(button => {
	button.addEventListener('click', () => {
		buttons.forEach(btn => {
			btn.classList.remove('blue');
		});

	    button.classList.add('blue');
	});
});